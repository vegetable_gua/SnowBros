import pygame

from SnowBros.Entity.BackgroundEntity import BackgroundEntity
from SnowBros.Entity.MonsterEntity import MonsterEntity
from SnowBros.Entity.PlayerAttackEntity import PlayerAttackEntity
from SnowBros.Entity.PlayerEntity import PlayerEntity
from SnowBros.Entity.SpriteRepository import SpriteRepository

SCREEN_RECT = pygame.Rect(0, 0, 506, 436)
# 刷新的帧率
FRAME_PER_SEC = 60


class SnowBrosEntry(object):

    def __init__(self):
        print("游戏初始化")
        """初始化音乐控件"""
        pygame.mixer.init()
        pygame.mixer_music.load("./Res/Music/清子 - FC雪人兄弟 第一关 BGM.flac")
        pygame.mixer_music.play(True)
        """游戏地图数据，用于玩家跳跃"""
        """键为高度，值为一个地形的两个端点"""

        # 0.初始化所有精灵
        SpriteRepository.initAllSpritesRes()
        # 1. 创建游戏的窗口
        self.Screen = pygame.display.set_mode(SCREEN_RECT.size)
        # 2. 创建游戏的时钟
        self.Clock = pygame.time.Clock()

        self.Monster = MonsterEntity(SpriteRepository.AllSpriteImageEntityDic[SpriteRepository.MON_RED_IDEL_1_KEY], 100,
                                     337)
        self.Monster_Group = pygame.sprite.Group(self.Monster)

        self.Monster2 = MonsterEntity(SpriteRepository.AllSpriteImageEntityDic[SpriteRepository.MON_RED_IDEL_1_KEY],
                                      300,
                                      202)
        self.Monster_Group.add(self.Monster2)

        self.Monster2 = MonsterEntity(SpriteRepository.AllSpriteImageEntityDic[SpriteRepository.MON_RED_IDEL_1_KEY],
                                      50,
                                      267)
        self.Monster_Group.add(self.Monster2)

        self.Monster2 = MonsterEntity(SpriteRepository.AllSpriteImageEntityDic[SpriteRepository.MON_RED_IDEL_1_KEY],
                                      100,
                                      135)
        self.Monster_Group.add(self.Monster2)

        self.Monster2 = MonsterEntity(SpriteRepository.AllSpriteImageEntityDic[SpriteRepository.MON_RED_IDEL_1_KEY],
                                      80,
                                      66)
        self.Monster_Group.add(self.Monster2)

        self.Player = PlayerEntity(SpriteRepository.AllSpriteImageEntityDic[SpriteRepository.PLAYER_IDEL_1_KEY])
        self.Player_Group = pygame.sprite.Group(self.Player)

        self.Bg1 = BackgroundEntity(SpriteRepository.AllSpriteImageEntityDic[SpriteRepository.BACKGROUND_KEY])
        self.Background_Group = pygame.sprite.Group(self.Bg1)

    def Start_Game(self):
        print("游戏开始...")
        while True:
            # 1. 设置刷新帧率
            self.Clock.tick(FRAME_PER_SEC)
            self.Event_Handler()
            self.Check_Collide()

            self.Background_Group.update()
            self.Background_Group.draw(self.Screen)

            self.Player_Group.update()
            self.Player_Group.draw(self.Screen)

            self.Monster_Group.update()
            self.Monster_Group.draw(self.Screen)

            self.Player.PlayerAttackEntity_Group.update()
            self.Player.PlayerAttackEntity_Group.draw(self.Screen)

            self.Player.FinalSnowBall_Group.update()
            self.Player.FinalSnowBall_Group.draw(self.Screen)

            # 5. 更新显示
            pygame.display.update()

    def Event_Handler(self):
        for event in pygame.event.get():
            # 判断是否退出游戏
            if event.type == pygame.QUIT:
                SnowBrosEntry.__game_over()
        # 使用键盘提供的方法获取键盘按键 - 按键元组
        keys_pressed = pygame.key.get_pressed()
        # 判断元组中对应的按键索引值 1
        if keys_pressed[pygame.K_a]:
            self.Player.AnimatorComponent.FlipByX = False
            self.Player.RunLeft()
        if keys_pressed[pygame.K_d]:
            self.Player.AnimatorComponent.FlipByX = True
            self.Player.RunRight()
        if keys_pressed[pygame.K_k]:
            self.Player.Jump()
        if keys_pressed[pygame.K_j]:
            self.Player.Attack()

    def Check_Collide(self):
        """先看子弹命中"""
        for playerAttackEntity in self.Player.PlayerAttackEntity_Group:
            for enemy in self.Monster_Group:
                if pygame.sprite.collide_mask(enemy, playerAttackEntity):
                    self.Player.CreateFinalSnowBall(enemy.rect.x, enemy.rect.y)
                    playerAttackEntity.kill()
                    enemy.kill()
            for finalSnowBallEntity in self.Player.FinalSnowBall_Group:
                if pygame.sprite.collide_mask(finalSnowBallEntity, playerAttackEntity):
                    playerAttackEntity.kill()

        """再看推雪球"""
        for finalSnowBallEntity in self.Player.FinalSnowBall_Group:
            if pygame.sprite.collide_mask(self.Player, finalSnowBallEntity):
                self.Player.CanPushSnowBall = True
                self.Player.CurrentFinalBallEntity = finalSnowBallEntity

            for enemyEntity in self.Monster_Group:
                if pygame.sprite.collide_mask(enemyEntity, finalSnowBallEntity):
                    enemyEntity.kill()

        """最后看英雄与敌人碰撞"""
        for enemy in self.Monster_Group:
            if pygame.sprite.collide_mask(self.Player, enemy):  # 这种碰撞检测可以精确到像素去掉alpha遮罩的那种哦
                self.Player.kill()

    @staticmethod
    def __start__():
        # 创建游戏对象
        game = SnowBrosEntry()
        # 启动游戏
        game.Start_Game()

    @staticmethod
    def __game_over():
        print("游戏结束")

        pygame.quit()
        exit()


if __name__ == '__main__':
    # 启动游戏
    SnowBrosEntry.__start__()
