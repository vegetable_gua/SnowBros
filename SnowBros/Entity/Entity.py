import pygame

from SnowBros.Component.AnimatorComponent import AnimatorComponent
from SnowBros.Entity.SpriteRepository import SpriteRepository


class Entity(pygame.sprite.Sprite):

    def __init__(self, image, x, y, width, height, flipByX):
        # 调用父类的初始化方法
        super().__init__()
        # 定义对象的属性
        self.Width = width
        self.Height = height
        self.FlipByX = flipByX

        self.AnimatorComponent = AnimatorComponent(self)
        """图片"""
        self.image = pygame.transform.scale(image, (width, height))
        # rect信息
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        # 是否需要翻转图片
        self.image = pygame.transform.flip(self.image, self.FlipByX, False)

    """更改精灵"""
    def SetSprite(self, spriteName):
        tempImage = SpriteRepository.AllSpriteImageEntityDic[spriteName]
        self.image = pygame.transform.scale(tempImage, (self.Width, self.Height))

    def update(self):
        self.AnimatorComponent.Update()
        if self.rect.x <= 0:
            self.rect.x = 0
            self.TryKillSelf()
        if self.rect.x >= 490:
            self.rect.x = 490
            self.TryKillSelf()

    def TryKillSelf(self):
        pass
