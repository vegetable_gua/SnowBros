import pygame
import random

from SnowBros.Entity.Entity import Entity
from SnowBros.Entity.MapDataEntity import MapDataEntity
from SnowBros.Entity.SpriteRepository import SpriteRepository


class MonsterEntity(Entity):
    def __init__(self, image, pos_x, pos_y):
        # 1. 调用父类方法实现精灵的创建(image/rect/speed)

        super().__init__(
            image, pos_x, pos_y,
            40, 34, False)

        self.IsJump = False
        self.IsRun = False
        self.ShouldDown = True
        self.ShouldRunLeft = False
        self.ShouldRunRight = False

        """起跳时间兼当前时间"""
        self.CurrentTime = 0
        """跳跃完成时间"""
        self.TargetJumpEndTime = 1

        """下一次跳跃时间"""
        self.AI_JumpTargetTime = 2
        """下一次转身时间"""
        self.AI_TurnTargetTime = 3

    def update(self):
        # 1. 调用父类的方法实现
        super().update()
        self.AI()
        if self.ShouldRunRight:
            self.RunRight()
        else:
            self.RunLeft()
        self.CurrentTime = pygame.time.get_ticks()
        self.CheckHeight()
        if self.IsJump:
            """确保不会空中二段跳"""
            if self.TargetJumpEndTime - self.CurrentTime >= 100:
                self.ShouldDown = False
            if self.CurrentTime < self.TargetJumpEndTime:
                self.rect.y -= 1
            else:
                self.IsJump = False

        if self.ShouldDown:
            self.rect.y += 2

    def RunLeft(self):
        self.FlipByX = False
        self.AnimatorComponent.PlayAnim(
            (SpriteRepository.MON_RED_IDEL_1_KEY,SpriteRepository.MON_RED_Run_1_KEY, SpriteRepository.MON_RED_IDEL_1_KEY),
            1000)
        self.rect.x -= 1
        if self.IsJump:
            self.IsRun = False
        else:
            self.IsRun = True

    def RunRight(self):
        self.FlipByX = True
        self.AnimatorComponent.PlayAnim(
            (SpriteRepository.MON_RED_IDEL_1_KEY,SpriteRepository.MON_RED_Run_1_KEY, SpriteRepository.MON_RED_IDEL_1_KEY),
            1000)
        self.rect.x += 1
        if self.IsJump:
            self.IsRun = False
        else:
            self.IsRun = True

    def Jump(self):
        if self.IsRun:
            self.AnimatorComponent.IsPlayAnimation = False
            self.IsRun = False
        if not self.IsJump and self.ShouldDown == False:
            self.AnimatorComponent.PlayAnim((SpriteRepository.MON_RED_JUMP_1_KEY,
                                             SpriteRepository.MON_RED_IDEL_1_KEY), 1300)
            self.IsJump = True
            self.StartJumpTime = pygame.time.get_ticks()
            """跳跃间隔1.3s"""
            self.TargetJumpEndTime = self.StartJumpTime + 1300

    """高度检测，用于判定英雄着陆点"""

    def CheckHeight(self):
        for i in MapDataEntity.MapData:
            """如果与地形离得很近，就判定是不是落在地形上，如果是就停止下落"""
            if 1 >= i - self.rect.y >= 0:
                """记录此高度所有x轴信息"""
                currentKeyValueLength = MapDataEntity.MapData[i].__len__()
                """存于tempGroundGroup中，键为组，每两个数据为一组，比如{1:[1,2],2:[1,2]}"""
                tempGroundGroup = {}
                tempGroundGroupFlag = 0
                temp = []
                for j in range(0, currentKeyValueLength):
                    temp.append(MapDataEntity.MapData[i][j])
                    if temp.__len__() % 2 == 0:
                        tempGroundGroup[tempGroundGroupFlag] = temp[tempGroundGroupFlag * 2:tempGroundGroupFlag * 2 + 2]
                        tempGroundGroupFlag += 1

                for k in tempGroundGroup:
                    if tempGroundGroup[k][1] >= self.rect.x >= tempGroundGroup[k][0]:
                        self.ShouldDown = False
                        return
                self.ShouldDown = True
                return
        self.ShouldDown = True

    """怪物AI，3秒一转身，2秒一跳跃"""

    def AI(self):
        if self.CurrentTime > self.AI_JumpTargetTime:
            self.Jump()
            self.AI_JumpTargetTime += random.randint(2000,3000)
        if self.CurrentTime > self.AI_TurnTargetTime:
            i = random.randint(0, 1)
            if i == 0:
                self.ShouldRunLeft = True
                self.ShouldRunRight = False
            else:
                self.ShouldRunLeft = False
                self.ShouldRunRight = True
            self.AI_TurnTargetTime += random.randint(2000,3000)
