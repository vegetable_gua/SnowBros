import pygame

class SpriteRepository:
    """背景图"""
    BACKGROUND_KEY = "Background"
    BACKGROUND_PATH_VALUE = "./Res/Sprites/BackGround/SnowBrosBackGround.png"
    """怪物图"""
    MON_RED_IDEL_1_KEY = "Mon_Red_Idel_1"
    MON_RED_IDEL_1_PATH_VALUE = "./Res/Sprites/Enemy/Mon_Red_1/Idel/Mon_Red_Idel_1.png"
    MON_RED_DIE_1_KEY = "Mon_Red_Die_1"
    MON_RED_DIE_1_KEY_PATH_VALUE = "./Res/Sprites/Enemy/Mon_Red_1/Die/Mon_Red_Die_1.png"
    MON_RED_JUMP_1_KEY = "Mon_Red_Jump_1"
    MON_RED_JUMP_1_KEY_PATH_VALUE = "./Res/Sprites/Enemy/Mon_Red_1/Jump/Mon_Red_Jump_1.png"
    MON_RED_Run_1_KEY = "Mon_Red_Run_1"
    MON_RED_Run_1_KEY_PATH_VALUE = "./Res/Sprites/Enemy/Mon_Red_1/Run/Mon_Red_Run_1.png"
    """玩家图"""
    PLAYER_IDEL_1_KEY = "Player_Idel_1"
    PLAYER_IDEL_1_PATH_VALUE = "./Res/Sprites/Player/Idel/Player_Idel_1.png"
    PLAYER_ATTACK_1_KEY = "Player_Attack_1"
    PLAYER_ATTACK_1_PATH_VALUE = "./Res/Sprites/Player/Attack/Player_Attack_1.png"
    PLAYER_ATTACK_2_KEY = "Player_Attack_2"
    PLAYER_ATTACK_2_PATH_VALUE = "./Res/Sprites/Player/Attack/Player_Attack_2.png"
    PLAYER_DIE_1_KEY = "Player_Die_1"
    PLAYER_DIE_1_PATH_VALUE = "./Res/Sprites/Player/Die/Player_Die_1.png"
    PLAYER_JUMP_1_KEY = "Player_Jump_1"
    PLAYER_JUMP_1_PATH_VALUE = "./Res/Sprites/Player/Jump/Player_Jump_1.png"
    PLAYER_JUMP_2_KEY = "Player_Jump_2"
    PLAYER_JUMP_2_PATH_VALUE = "./Res/Sprites/Player/Jump/Player_Jump_2.png"
    PLAYER_JUMP_3_KEY = "Player_Jump_3"
    PLAYER_JUMP_3_PATH_VALUE = "./Res/Sprites/Player/Jump/Player_Jump_3.png"
    PLAYER_JUMP_4_KEY = "Player_Jump_4"
    PLAYER_JUMP_4_PATH_VALUE = "./Res/Sprites/Player/Jump/Player_Jump_4.png"
    PLAYER_RUN_1_KEY = "Player_Run_1"
    PLAYER_RUN_1_PATH_VALUE = "./Res/Sprites/Player/Run/Player_Run_1.png"
    PLAYER_RUN_2_KEY = "Player_Run_2"
    PLAYER_RUN_2_PATH_VALUE = "./Res/Sprites/Player/Run/Player_Run_2.png"
    """玩家发射的飞镖图"""
    PLAYER_ATTACKENTITY_1_KEY = "Player_AttackEntity_1"
    PLAYER_ATTACKENTITY_1_PATH_VALUE = "./Res/Sprites/PlayerAttack/Player_AttackEntity_1.png"
    PLAYER_ATTACKENTITY_2_KEY = "Player_AttackEntity_2"
    PLAYER_ATTACKENTITY_2_PATH_VALUE = "./Res/Sprites/PlayerAttack/Player_AttackEntity_2.png"
    """雪球图"""
    FINALSNOWBALL_KEY = "FinalSnowBall"
    FINALSNOWBALL_KEY_PATH_VALUE = "./Res/Sprites/FinalSnowBall/FinalSnowBall.png"

    AllSpriteImagePathDic = {
        BACKGROUND_KEY: BACKGROUND_PATH_VALUE,

        MON_RED_IDEL_1_KEY: MON_RED_IDEL_1_PATH_VALUE,
        MON_RED_DIE_1_KEY: MON_RED_DIE_1_KEY_PATH_VALUE,
        MON_RED_JUMP_1_KEY: MON_RED_JUMP_1_KEY_PATH_VALUE,
        MON_RED_Run_1_KEY: MON_RED_Run_1_KEY_PATH_VALUE,

        PLAYER_IDEL_1_KEY: PLAYER_IDEL_1_PATH_VALUE,
        PLAYER_ATTACK_1_KEY: PLAYER_ATTACK_1_PATH_VALUE,
        PLAYER_ATTACK_2_KEY: PLAYER_ATTACK_2_PATH_VALUE,
        PLAYER_DIE_1_KEY: MON_RED_DIE_1_KEY_PATH_VALUE,
        PLAYER_JUMP_1_KEY: PLAYER_JUMP_1_PATH_VALUE,
        PLAYER_JUMP_2_KEY: PLAYER_JUMP_2_PATH_VALUE,
        PLAYER_JUMP_3_KEY: PLAYER_JUMP_3_PATH_VALUE,
        PLAYER_JUMP_4_KEY: PLAYER_JUMP_4_PATH_VALUE,
        PLAYER_RUN_1_KEY: PLAYER_RUN_1_PATH_VALUE,
        PLAYER_RUN_2_KEY: PLAYER_RUN_2_PATH_VALUE,

        PLAYER_ATTACKENTITY_1_KEY: PLAYER_ATTACKENTITY_1_PATH_VALUE,
        PLAYER_ATTACKENTITY_2_KEY: PLAYER_ATTACKENTITY_2_PATH_VALUE,

        FINALSNOWBALL_KEY:FINALSNOWBALL_KEY_PATH_VALUE}

    AllSpriteImageEntityDic = {}

    @staticmethod
    def initAllSpritesRes():
        for key in SpriteRepository.AllSpriteImagePathDic:
            SpriteRepository.AllSpriteImageEntityDic[key] = pygame.image.load(
                SpriteRepository.AllSpriteImagePathDic[key])
