import pygame

from SnowBros.Entity.Entity import Entity


class BackgroundEntity(Entity):
    """游戏背景精灵"""

    def __init__(self, image):
        # 1. 调用父类方法实现精灵的创建(image/rect/speed)
        super().__init__(image, 0,
                         0, 506,
                         436,False)

    def update(self):
        # 1. 调用父类的方法实现
        super().update()
