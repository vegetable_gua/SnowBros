import pygame

from SnowBros.Entity.MapDataEntity import MapDataEntity
from SnowBros.Entity.FinalSnowBallEntity import FinalSnowBallEntity
from SnowBros.Entity.PlayerAttackEntity import PlayerAttackEntity
from SnowBros.Entity.SpriteRepository import SpriteRepository
from SnowBros.Entity.Entity import Entity


class PlayerEntity(Entity):
    def __init__(self, image):
        # 1. 调用父类方法实现精灵的创建(image/rect/speed)
        super().__init__(image, 300, 337,
                         26,
                         36, False)
        # 3. 创建子弹和雪球的精灵组
        self.PlayerAttackEntity_Group = pygame.sprite.Group()
        self.FinalSnowBall_Group = pygame.sprite.Group()
        """是否已经跳起"""
        self.IsJump = False
        """是否正在奔跑"""
        self.IsRun = False
        """是否应该受重力影响"""
        self.ShouldDown = True
        """是否可以推雪球"""
        self.CanPushSnowBall = False
        """当前持有的雪球"""
        self.CurrentFinalBallEntity = None

        """起跳时间兼当前时间"""
        self.CurrentTime = 0
        """下一次攻击时间"""
        self.TargetAttackTime = 2
        """跳跃完成时间"""
        self.TargetJumpEndTime = 1

    def update(self):
        # 1. 调用父类的方法实现
        super().update()
        self.CurrentTime = pygame.time.get_ticks()
        self.CheckHeight()
        if self.IsJump:
            """确保不会空中二段跳"""
            if self.TargetJumpEndTime - self.CurrentTime >= 100:
                self.ShouldDown = False
            if self.CurrentTime < self.TargetJumpEndTime:
                self.rect.y -= 1
            else:
                self.IsJump = False

        if self.ShouldDown:
            self.rect.y += 2

    def RunLeft(self):
        self.FlipByX = False
        self.AnimatorComponent.PlayAnim(
            (SpriteRepository.PLAYER_RUN_1_KEY, SpriteRepository.PLAYER_RUN_2_KEY, SpriteRepository.PLAYER_IDEL_1_KEY),
            500)
        self.rect.x -= 1
        if self.IsJump:
            self.IsRun = False
        else:
            self.IsRun = True

    def RunRight(self):
        self.FlipByX = True
        self.AnimatorComponent.PlayAnim(
            (SpriteRepository.PLAYER_RUN_1_KEY, SpriteRepository.PLAYER_RUN_2_KEY, SpriteRepository.PLAYER_IDEL_1_KEY),
            500)
        self.rect.x += 1
        if self.IsJump:
            self.IsRun = False
        else:
            self.IsRun = True

    def Jump(self):
        if self.IsRun:
            self.AnimatorComponent.IsPlayAnimation = False
            self.IsRun = False
        if not self.IsJump and self.ShouldDown == False:
            self.AnimatorComponent.PlayAnim((SpriteRepository.PLAYER_JUMP_1_KEY,
                                             SpriteRepository.PLAYER_JUMP_2_KEY,
                                             SpriteRepository.PLAYER_JUMP_3_KEY,
                                             SpriteRepository.PLAYER_JUMP_4_KEY,
                                             SpriteRepository.PLAYER_IDEL_1_KEY), 1300)
            self.IsJump = True
            self.StartJumpTime = pygame.time.get_ticks()
            """跳跃间隔1.3s"""
            self.TargetJumpEndTime = self.StartJumpTime + 1300

    """高度检测，用于判定英雄着陆点"""

    def CheckHeight(self):
        for i in MapDataEntity.MapData:
            """如果与地形离得很近，就判定是不是落在地形上，如果是就停止下落"""
            if 1 >= i - self.rect.y >= 0:
                """记录此高度所有x轴信息"""
                currentKeyValueLength = MapDataEntity.MapData[i].__len__()
                """存于tempGroundGroup中，键为组，每两个数据为一组，比如{1:[1,2],2:[1,2]}"""
                tempGroundGroup = {}
                tempGroundGroupFlag = 0
                temp = []
                for j in range(0, currentKeyValueLength):
                    temp.append(MapDataEntity.MapData[i][j])
                    if temp.__len__() % 2 == 0:
                        tempGroundGroup[tempGroundGroupFlag] = temp[tempGroundGroupFlag * 2:tempGroundGroupFlag * 2 + 2]
                        tempGroundGroupFlag += 1

                print("玩家当前位置x:{0}  y:{1},将要判定高度：{2},将要判定水平：{3}，收集到的判定结果为{4}".format(self.rect.x, self.rect.y, i,
                                                                                     MapDataEntity.MapData[i],
                                                                                     tempGroundGroup))
                for k in tempGroundGroup:
                    if tempGroundGroup[k][1] >= self.rect.x >= tempGroundGroup[k][0]:
                        self.ShouldDown = False
                        return
                self.ShouldDown = True
                return
        self.ShouldDown = True

    def Attack(self):
        if self.CurrentFinalBallEntity is not None and self.CanPushSnowBall:
            dir = self.CurrentFinalBallEntity.rect.x - self.rect.x
            self.CurrentFinalBallEntity.Move(dir)
            self.CanPushSnowBall = False
            return

        if self.CurrentTime > self.TargetAttackTime:
            self.AnimatorComponent.PlayAnim((SpriteRepository.PLAYER_ATTACK_1_KEY,
                                             SpriteRepository.PLAYER_ATTACK_2_KEY,
                                             SpriteRepository.PLAYER_IDEL_1_KEY), 300)
            bullet = PlayerAttackEntity(
                SpriteRepository.AllSpriteImageEntityDic[SpriteRepository.PLAYER_ATTACKENTITY_1_KEY], self.rect.x,
                self.rect.y, self.FlipByX)
            self.PlayerAttackEntity_Group.add(bullet)
            """攻击间隔0.3s"""
            self.TargetAttackTime = pygame.time.get_ticks() + 300

    def CreateFinalSnowBall(self, pos_x, pos_y):
        finalSnowBall = FinalSnowBallEntity(
            SpriteRepository.AllSpriteImageEntityDic[SpriteRepository.FINALSNOWBALL_KEY], pos_x, pos_y)
        self.FinalSnowBall_Group.add(finalSnowBall)
