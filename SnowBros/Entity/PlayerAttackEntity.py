import pygame

from SnowBros.Entity.Entity import Entity
from SnowBros.Entity.SpriteRepository import SpriteRepository


class PlayerAttackEntity(Entity):
    def __init__(self, image, pos_x, pos_y, flipByX):
        # 1. 调用父类方法实现精灵的创建(image/rect/speed)

        super().__init__(
            image, pos_x, pos_y,
            15, 20, flipByX)
        # 目前时间
        self.CurrentTime = pygame.time.get_ticks()
        # 目标消融时间
        self.TargetDieTime = self.CurrentTime+1000

    def update(self):
        # 1. 调用父类的方法实现
        self.CurrentTime = pygame.time.get_ticks()
        super().update()
        if self.CurrentTime < self.TargetDieTime:
            self.AnimatorComponent.PlayAnim(
                (SpriteRepository.PLAYER_ATTACKENTITY_1_KEY, SpriteRepository.PLAYER_ATTACKENTITY_2_KEY,
                SpriteRepository.PLAYER_ATTACKENTITY_1_KEY), 500)
            if self.FlipByX:
                self.rect.x += 3
            else:
                self.rect.x -= 3
        else:
            self.kill()

    def TryKillSelf(self):
        self.kill()
